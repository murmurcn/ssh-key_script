#!/bin/bash
echo "------------------------------------------------------"
echo "|      Welcome to Murmur CHINA SSH setting tool      |"
echo "|     https://gitlab.com/murmurcn/ssh-key_script     |"
echo "------------------------------------------------------"
echo "|This setting tool will help you to import the SSH   |"
echo "|public key to the server. It is optional to disable |"
echo "|SSH login via password authentication.              |"
echo "------------------------------------------------------"
echo "|                      WARNING!                      |"
echo "|PLEASE RUN THIS TOOL IN SUDO MODE OR ROOT USER.     |"
echo "------------------------------------------------------"

echo -n "Press any key to continue!"
read -s press
echo ""

# Question time
echo "Which user name you want to use? (default: root)"
echo -n "Username: "; read uname
if [ -z $uname ]; then
    uname="root"
fi
echo "Saved."

echo -n "Do you want to keep SSH login via password authentication? (Y/n) "; read passwd_q
if [ -z $passwd_q ]; then
	passwd_q="y"
fi
echo "Saved."

# Magic time
echo "------------------------------------------------------"
echo "|        Setting your SSH public key now...          |"
echo "------------------------------------------------------"

dir="$HOME/.ssh/authorized_keys"

echo "Checking if ~/.ssh/authorized_keys exist..."
ls $dir
if [ $? != 0 ]; then
    echo "~/.ssh/authorized_keys does not exist. Creating a new one..."
    touch $HOME/.ssh/authorized_keys
else
    echo "Great! ~/.ssh/authorized_keys already exist."
fi

pubkey="ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAnr5pVBJK/yNkKfY63ge7LJrbAu3+A/BTNMTiz0mZzdvk2CFGq6K2rf2pohJFBfdsxQdcFf7ekUdemxjQ/p9k/WdjbkUEKfNHIRBAqzWMVuxhOELiz7hs47hkmfZb79r9nlCfFV9dlnkzSrQ0/n3W9CWu5VH0iys5eIEP2TxL63RCOSIZIe5/daoUFgSl3tjJ6abszg8VaFc/Y0O7S50eHZIB3S6CEevtlMdo74bkLmI+QYoRePhpBMpJXjICm/7QbcgT/2ay8uacubIsAt3pHumnLISJyjfC9q8mk+LjHToXjE4FcwO+sYsm7KgvqcHBL5QLnetf+GAM6aH4o4JLyQ== $uname"

grep -w $pubkey $dir 
if [ $? != 0 ]; then
    echo "Adding new public key..."
    echo "$pubkey" >> $dir
else
    echo "The public key has existed already."
fi

if [[ $passwd_q =~ ^Y$|^y$ ]]; then
    ls /etc/ssh/sshd_config
    if [ $? == 0 ]; then
        sed -i "s/^#PasswordAuthentication yes$/PasswordAuthentication no/" /etc/ssh/sshd_config
        sed -i "s/^UsePAM yes$/UsePAM no/" /etc/ssh/sshd_config
        service ssh restart && service sshd restart
        echo "SSH server has restart. Please keep this connection and open a new SSH connection via pubkey to test. "
    else
        echo "/etc/ssh/sshd_config does not exist! Skipping this step..."
    fi
fi

echo "------------------------------------------------------"
echo "|          All done! See you next time :)            |"
echo "------------------------------------------------------"
echo "Press [any] to continue..."; read -s -N 1 key
exit 0